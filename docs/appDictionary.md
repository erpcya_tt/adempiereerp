# Introducción al Diccionario de Aplicación

El diccionario de aplicación es uno de los aspectos más importantes de Adempiere, prácticamente la totalidad de la aplicación se puede manejar a través de los cambios en el diccionario reduciendo considerablemente el desarrollo de código. La mayor parte de la aplicación, se puede configurar directamente en el diccionario, sin necesidad de compilación o reconstrucción del mismo.

En esta sección se describe:

* Tipo de Entidad

* Elemento

* Tabla y Columnas

* Ventana, pestaña y campos.

# Estructura de las Ventanas

ADempiere maneja un diseño único para su ventanas, en este capítulo se explicará la estructura de las mismas: 

1. **Barra de Herramientas**

2. **Pestañas**

3. **Barra de Información**

## Barra de Herramientas

ADempiere cuenta con una barra de herramientas con la que interactúa en la mayoría de las ventanas del Sistema.A continuación se explica:

![image alt text](img/image_5.png)

## Definición de Iconos

<table>
  <tr>
    <td>N</td>
    <td>Icono</td>
    <td>Nombre</td>
    <td>Descripción</td>
  </tr>
  <tr>
    <td>1</td>
    <td></td>
    <td>Ignorar Cambios</td>
    <td>Deshacer cambios realizados</td>
  </tr>
  <tr>
    <td>2</td>
    <td></td>
    <td>Ayuda</td>
    <td>Podrá acceder </td>
  </tr>
  <tr>
    <td>3
</td>
    <td></td>
    <td>Registro Nuevo</td>
    <td>Crear un nuevo registro.</td>
  </tr>
  <tr>
    <td>4</td>
    <td></td>
    <td>Copiar Registro</td>
    <td>Copiar un registro a partir de un registro existente.</td>
  </tr>
  <tr>
    <td>5</td>
    <td></td>
    <td>Borrar Registro</td>
    <td>Eliminar un registro seleccionado.</td>
  </tr>
  <tr>
    <td>6</td>
    <td></td>
    <td>Borrar Artículos Seleccionado</td>
    <td>Eliminar multiples registros.</td>
  </tr>
  <tr>
    <td>7</td>
    <td></td>
    <td>Guardar Cambios</td>
    <td>Guardar un registro nuevo o un registro modificado.</td>
  </tr>
  <tr>
    <td>8</td>
    <td></td>
    <td>Refrescar</td>
    <td>Refrescar los registros.</td>
  </tr>
  <tr>
    <td>9</td>
    <td></td>
    <td>Encontrar Registro</td>
    <td>Buscar un registro en específico.</td>
  </tr>
  <tr>
    <td>10</td>
    <td></td>
    <td>Anexo</td>
    <td>Adjuntar archivo a un registro(Imagen,Documento, entre otros).</td>
  </tr>
  <tr>
    <td>11</td>
    <td></td>
    <td>Comentarios</td>
    <td>Podrá realizar comentarios a un registro.</td>
  </tr>
  <tr>
    <td>12</td>
    <td></td>
    <td>Cambiar Multi/Mono Registro</td>
    <td>Cambio de vista a modo lista o modo unica vista.</td>
  </tr>
  <tr>
    <td>13</td>
    <td></td>
    <td>Registros Históricos</td>
    <td>Podrá ver un histórico de los registros (Día, Semana, Mes,Año y Todos) </td>
  </tr>
  <tr>
    <td>14</td>
    <td></td>
    <td>Menú</td>
    <td>Podrá ver la ventana principal de ADempiere</td>
  </tr>
  <tr>
    <td>15</td>
    <td></td>
    <td>Pestaña Anterior</td>
    <td>Podrá ir a la pestaña anterior de la ventana.</td>
  </tr>
  <tr>
    <td>16</td>
    <td></td>
    <td>Pestaña Próxima</td>
    <td>Podrá ir a la próxima pestaña de la ventana.</td>
  </tr>
  <tr>
    <td>17</td>
    <td></td>
    <td>Primer Registro</td>
    <td>Podrá ir al primer registro de todos.</td>
  </tr>
  <tr>
    <td>18</td>
    <td></td>
    <td>Registro Anterior</td>
    <td>Podrá ir al registro anterior de los registros.</td>
  </tr>
  <tr>
    <td>19</td>
    <td></td>
    <td>Registro Próximo</td>
    <td>Podrá ir al registro próximo de los registros.</td>
  </tr>
  <tr>
    <td>20</td>
    <td></td>
    <td>Último Registro</td>
    <td>Podrá ir al último registro de todos.</td>
  </tr>
  <tr>
    <td>21</td>
    <td></td>
    <td>Informe</td>
    <td>Crear un informe fácil de todos los registros existentes.</td>
  </tr>
  <tr>
    <td>22</td>
    <td></td>
    <td>Documentos e Informes Archivados</td>
    <td>Archivar y Guardar los informes creados.</td>
  </tr>
  <tr>
    <td>23</td>
    <td></td>
    <td>Vista Previa</td>
    <td>Previsualizar el documento</td>
  </tr>
  <tr>
    <td>24</td>
    <td></td>
    <td>Imprimir</td>
    <td>Imprimir.</td>
  </tr>
  <tr>
    <td>25</td>
    <td></td>
    <td>Visualiza Detalle(Donde es Usado)</td>
    <td>Podrá ir a los registros en los que esté referenciado este registro. </td>
  </tr>
  <tr>
    <td>26</td>
    <td></td>
    <td>Flujo de Trabajo Activos</td>
    <td>Podrá ver actividades o procesos pendiente.</td>
  </tr>
  <tr>
    <td>27</td>
    <td></td>
    <td>Chequee Solicitudes</td>
    <td>Solicitudes de Contactos.</td>
  </tr>
  <tr>
    <td>28</td>
    <td></td>
    <td>Proceso</td>
    <td>Genera un proceso asociado.</td>
  </tr>
  <tr>
    <td>29</td>
    <td></td>
    <td>Información de Producto</td>
    <td>Podrá acceder a una consulta de producto.</td>
  </tr>
  <tr>
    <td>30</td>
    <td></td>
    <td>Finalizar Ventana</td>
    <td>Cierra la Ventana</td>
  </tr>
</table>


**Pestañas:**

        Permite cambiar rápidamente lo que se está viendo sin cambiar de ventana que se usa en ADempiere. Una particularidad que tiene este tipo de diseño es que se pueden agregar pestañas dependientes de otras.

![image alt text](img/image_6.png)

1. Pestaña Padre.

2. Pestaña Dependiente.

**Barra de Información:**

La barra de información  está ubicada en la parte inferior de la ventana. Se divide en dos partes como se muestra en la siguiente imagen.

![image alt text](img/image_7.png)

1. Muestra Información referente a la ventana; acciones realizadas, mensajes informativos, de alertas y errores.

2. Indica el registro donde está posicionado y la cantidad de registros existentes para la ventana.
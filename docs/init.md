# Tipo de Entidad

![image alt text](img/image_9.png)

El tipo de entidad determina la propiedad de las entradas del diccionario de aplicación.

En ADempiere el tipo de entidad "USER MAINTAINED" protegerá las tablas que se creen para que no sean sobrescritas en futuras migraciones. Las tablas creadas con el tipo de entidad "DICTIONARY" son sujeto de migraciones. 

**Nota:**** **Los tipos de entidad "Dictionary" y "Adempiere" no deben ser utilizados  debido a que este es el diccionario oficial de la comunidad de Adempiere. El tipo de entidad, también es utilizado para buscar las clases del modelo en base a los tipos de entidad (ej. org.compiere.model).

**Ventana de Tipo de Entidad:**

![image alt text](img/image_10.png)

**Datos de Ejemplo:**

<table>
  <tr>
    <td>Nombre</td>
    <td>Descripción</td>
    <td>Ejemplo</td>
  </tr>
  <tr>
    <td>Cliente</td>
    <td>Cliente / Inquilino para esta instalación.</td>
    <td>System</td>
  </tr>
  <tr>
    <td>Organización</td>
    <td>Entidad de organización dentro del cliente</td>
    <td>*</td>
  </tr>
  <tr>
    <td>Tipo de entidad</td>
    <td>Diccionario del tipo de entidad;Determina propiedad y sincronización</td>
    <td>ERP_Curso</td>
  </tr>
  <tr>
    <td>Nombre</td>
    <td>Identificador alfanumérico de la entidad</td>
    <td>Curso</td>
  </tr>
  <tr>
    <td>Descripción</td>
    <td>Descripción corta opcional del registro</td>
    <td></td>
  </tr>
  <tr>
    <td>Comentario / Ayuda</td>
    <td>Comentario o Sugerencia</td>
    <td></td>
  </tr>
  <tr>
    <td>Activo</td>
    <td>El registro está activo en el sistema</td>
    <td>Si</td>
  </tr>
  <tr>
    <td>Versión</td>
    <td>Versión de la definición de la tabla</td>
    <td></td>
  </tr>
  <tr>
    <td>Paquete Modelo</td>
    <td>El paquete de Java de las clases del modelo</td>
    <td>org.curso.model</td>
  </tr>
</table>


# Elemento

![image alt text](img/image_11.png)

Es el repositorio central para el nombre de los campos, descripciones y comentarios/ayudas, así como también sus traducciones. Define los nombres, etiquetas, descripciones y ayudas dependiendo del contexto. Son mantenidos centralmente (al "Sincronizar la Terminología" del sistema todos los campos toman la traducción que tenga su elemento). Si necesita una excepción se debe deshabilitar la opción "Centrally Maintained" (mantenido centralmente) en el campo (Ventana, Pestaña & Campos).

** Ventana de Elemento:**

![image alt text](img/image_12.png)

**Ejemplo:**

Para la creación de un elemento se requiere:

Nota: Los campos marcados con "*" son obligatorios.

<table>
  <tr>
    <td>Nombre</td>
    <td>Descripción</td>
    <td>Ejemplo</td>
  </tr>
  <tr>
    <td>Compañía *</td>
    <td>Compañía para esta instalación</td>
    <td>System</td>
  </tr>
  <tr>
    <td>Organización *</td>
    <td>Entidad organizacional dentro de la compañía</td>
    <td>*</td>
  </tr>
  <tr>
    <td>Nombre de Columna en BD *</td>
    <td>Nombre de la columna en la base de datos </td>
    <td>IsInkeeper</td>
  </tr>
  <tr>
    <td>Nombre *</td>
    <td>Identificador alfanumérico de la entidad. </td>
    <td>Inkeeper</td>
  </tr>
  <tr>
    <td>Nombre a ser Impreso *</td>
    <td>Indica el nombre a ser impreso en un documento o correspondencia</td>
    <td>Inkeeper</td>
  </tr>
  <tr>
    <td>Descripción</td>
    <td>Descripción corta opcional del registro </td>
    <td></td>
  </tr>
  <tr>
    <td>Comentario</td>
    <td>Ayuda; Comentario o Sugerencia </td>
    <td></td>
  </tr>
  <tr>
    <td>Activo</td>
    <td>El registro está activo en el sistema </td>
    <td>Si</td>
  </tr>
  <tr>
    <td>Tipo de Entidad *</td>
    <td>Tipo de Entidad Diccionario; determina propiedad y sincronización </td>
    <td>Curso</td>
  </tr>
  <tr>
    <td>Referencia</td>
    <td>Referencia del Sistema y Validación</td>
    <td>Si/No</td>
  </tr>
  <tr>
    <td>Longitud</td>
    <td>Longitud de la columna en la base de datos</td>
    <td>1</td>
  </tr>
  <tr>
    <td>Nombre en OC</td>
    <td>Nombre en pantalla de orden de compras </td>
    <td></td>
  </tr>
  <tr>
    <td>Nombre a Imprimir en OC</td>
    <td>Nombre a Imprimir en OC</td>
    <td></td>
  </tr>
  <tr>
    <td>Descripción en OC</td>
    <td>Descripción en OC</td>
    <td></td>
  </tr>
  <tr>
    <td>Ayuda en OC</td>
    <td>Ayuda en OC</td>
    <td></td>
  </tr>
</table>


**Pestaña de Traducción:**

<table>
  <tr>
    <td>Nombre</td>
    <td>Descripción</td>
    <td>Ejemplo</td>
  </tr>
  <tr>
    <td>Cliente</td>
    <td>Cliente / Inquilino para esta instalación.</td>
    <td>System</td>
  </tr>
  <tr>
    <td>Organización</td>
    <td>Entidad de organización dentro del cliente</td>
    <td>*</td>
  </tr>
  <tr>
    <td>Elemento sistema</td>
    <td>Elemento sistema permite el mantenimiento central de la descripción de la columna y ayuda.</td>
    <td>IsInkeeper</td>
  </tr>
  <tr>
    <td>Idioma</td>
    <td>Lenguaje para esta</td>
    <td>Español</td>
  </tr>
  <tr>
    <td>Activo</td>
    <td>El registro está activo en el sistema</td>
    <td>Si</td>
  </tr>
  <tr>
    <td>Traducido</td>
    <td>Indica si esta columna está traducida</td>
    <td>No</td>
  </tr>
  <tr>
    <td>Nombre</td>
    <td>identificador alfanumérico de la entidad</td>
    <td>Mesonero</td>
  </tr>
  <tr>
    <td>Imprimir texto</td>
    <td>El texto de la etiqueta que se va a imprimir un documento o correspondencia.</td>
    <td>Mesonero</td>
  </tr>
  <tr>
    <td>Descripción</td>
    <td>Descripción corta opcional del registro</td>
    <td>
</td>
  </tr>
  <tr>
    <td>Comentario / Ayuda</td>
    <td>Comentario o Sugerencia</td>
    <td></td>
  </tr>
  <tr>
    <td>Nombre PO</td>
    <td>Nombre que aparece en las pantallas de PO</td>
    <td></td>
  </tr>
  <tr>
    <td>PO nombre Imprimir</td>
    <td>Nombre de impresión en la PO Pantallas / Informes</td>
    <td></td>
  </tr>
  <tr>
    <td>Descripción PO</td>
    <td>Descripción de las pantallas de PO</td>
    <td></td>
  </tr>
  <tr>
    <td>Ayuda PO</td>
    <td>Ayuda para pantallas PO</td>
    <td></td>
  </tr>
</table>


**	**

**Referencia**

 	La Referencia indica el tipo de campo a desplegar, lista o validación de tabla  en la ventana. La siguiente tabla muestra una lista de los tipos de campos existente manejados por ADempiere.

<table>
  <tr>
    <td>Type</td>
    <td>Tipo</td>
    <td>Descripción</td>
  </tr>
  <tr>
    <td>Memo

</td>
    <td>Memo
</td>
    <td>Editor de texto, permite hasta 2000 caracteres</td>
  </tr>
  <tr>
    <td>Product Attribute</td>
    <td>Atributos del producto</td>
    <td>los atributos del producto</td>
  </tr>
  <tr>
    <td>Text Long</td>
    <td>Texto largo</td>
    <td>Texto largo</td>
  </tr>
  <tr>
    <td>Binary</td>
    <td>Binario</td>
    <td>datos binarios</td>
  </tr>
  <tr>
    <td>Time</td>
    <td>Tiempo</td>
    <td>Hora</td>
  </tr>
  <tr>
    <td>Location</td>
    <td>Ubicación (dirección)</td>
    <td>Lugar / Dirección</td>
  </tr>
  <tr>
    <td>Date</td>
    <td>Fecha</td>
    <td>Fecha dd / mm / aaaa</td>
  </tr>
  <tr>
    <td>Yes-No</td>
    <td>Sí-No</td>
    <td>casilla de verificación</td>
  </tr>
  <tr>
    <td>Number</td>
    <td>Número</td>
    <td>Número Flotante</td>
  </tr>
  <tr>
    <td>Integer</td>
    <td>Entero</td>
    <td>10 dígito numérico</td>
  </tr>
  <tr>
    <td>FilePath</td>
    <td>FilePath</td>
    <td>Ruta del archivo local</td>
  </tr>
  <tr>
    <td>FileName</td>
    <td>Nombre de archivo</td>
    <td>Archivo local</td>
  </tr>
  <tr>
    <td>Printer Name</td>
    <td>Nombre de la impresora</td>
    <td>Nombre de la impresora</td>
  </tr>
  <tr>
    <td>URL</td>
    <td>URL</td>
    <td>URL</td>
  </tr>
  <tr>
    <td>RowID</td>
    <td>ROWID</td>
    <td>Tipo de fila de datos de identificación</td>
  </tr>
  <tr>
    <td>Color</td>
    <td>Color</td>
    <td>elemento de color</td>
  </tr>
  <tr>
    <td>Image</td>
    <td>Imagen</td>
    <td>datos de imagen binaria</td>
  </tr>
  <tr>
    <td>Account
</td>
    <td>Cuenta</td>
    <td>Elemento de Cuenta</td>
  </tr>
  <tr>
    <td>Locator (WH)</td>
    <td>Localizador (WH)</td>
    <td>tipo de almacén de datos de localización</td>
  </tr>
  <tr>
    <td>Button</td>
    <td>Botón</td>
    <td>botones de comando - inicia un proceso de</td>
  </tr>
  <tr>
    <td>Assignment</td>
    <td>Asignación</td>
    <td>Asignación de Recursos</td>
  </tr>
  <tr>
    <td>Chart</td>
    <td>Tabla</td>
    <td>Grafico</td>
  </tr>
  <tr>
    <td>FilePathOrName</td>
    <td>FilePathOrName</td>
    <td>Ruta del archivo local o Nombre</td>
  </tr>
  <tr>
    <td>Costs+Prices</td>
    <td>Costos + Precios</td>
    <td>precisión mínima de divisas, pero si existe más</td>
  </tr>
  <tr>
    <td>Quantity</td>
    <td>Cantidad</td>
    <td>Campo Numérico</td>
  </tr>
  <tr>
    <td>Table</td>
    <td>Tabla</td>
    <td>Tabla Lista</td>
  </tr>
  <tr>
    <td>Amount</td>
    <td>Monto</td>
    <td>Número con 4 decimales</td>
  </tr>
  <tr>
    <td>List</td>
    <td>Lista</td>
    <td>Lista de referencias</td>
  </tr>
  <tr>
    <td>String</td>
    <td>Cadena</td>
    <td>cadena de caracteres</td>
  </tr>
  <tr>
    <td>Search</td>
    <td>Buscar</td>
    <td>Campo de búsqueda</td>
  </tr>
  <tr>
    <td>Table Direct</td>
    <td>Tabla Directa</td>
    <td>Tabla de acceso directo</td>
  </tr>
  <tr>
    <td>ID</td>
    <td>ID</td>
    <td>Identificador de 10 dígito</td>
  </tr>
  <tr>
    <td>Date+Time</td>
    <td>Fecha + Hora</td>
    <td>Fecha con el tiempo</td>
  </tr>
  <tr>
    <td>Text</td>
    <td>Texto</td>
    <td>cadena de caracteres hasta 2000 caracteres</td>
  </tr>
</table>


# Tabla y Columna

![image alt text](img/image_13.png)

Las tablas y columnas se consideran metadata de las tablas de la base de datos traducida a la notación de ADempiere. Estas se pueden caracterizar de la siguiente forma:

## Tabla

![image alt text](img/image_14.png)

**Descripción de los Campos:**

* El nombre de la tabla es sensible a mayúsculas y minúsculas. Al registrar una tabla en el diccionario debe tenerse en cuenta la siguiente estructura para el registro: PREFIJO_Nombre_De_Tabla

* El ID de la tabla debe tener el nombre exacto de la tabla + el sufijo ID: PREFIJO_Nombre_De_Tabla_ID

* View (Vista): Esta opción permite definir la tabla como una vista, no se necesita sincronizar con la base de datos. Se puede usar la opción "vista" para hacer la tabla de solo-lectura.

* Data Access Level (Nivel de Acceso de Datos): Usado para definir el acceso por defecto para los roles. Generalmente se coloca como Client + Organization ("Compañía + Organización")

* Maintain Change Log (Mantiene Bitácora de Cambios): Cuando se selecciona esta opción todos los cambios hechos a esta tabla son guardados en la tabla AD_ChangeLog.

* Window (Ventana): Define la ventana a la que se accederá cuando se haga un acercamiento en el campo identificador de esta tabla. Además activa la funcionalidad "zoom" (acercar). Se puede definir una ventana diferente para los procesos de compras (PO Window (Ventana OC))

* Records deleteable (Registros Eliminables): Se utiliza para habilitar o deshabilitar el borrado de registros en la base de datos.

* High Volume (Volumen Alto): Indica si se mostrará la ventana de búsqueda para seleccionar registros de esta tabla.

* Auto Complete Min Length: Se utiliza para definir la cantidad de letra para aparecer en el campo de busqueda en el menú.

* Is Document: Indica si la ventana es utilizada como un documento.

* Entity Type (Tipo de Entidad): Se indica la entidad a la cual pertenece el registro (la tabla). En el caso de los desarrollos aplicados en este manual se utilizará "Curso".

* Create Columns from DB (Crear Columnas desde Base de Datos): Si se crea una tabla en la base de datos o realizar cualquier cambio en alguna tabla (ALTER TABLE ADD/MODIFY) se pueden traer los cambios al diccionario mediante esta opción.

* Copy Columns From Table (Copiar columnas desde la tabla): Este es el proceso más rápido para crear una tabla. Permite seleccionar una tabla similar a la que se debe crear y este proceso traerá las columnas exactas como la tiene la otra tabla renombrando el ID para que se corresponda con el nombre de la tabla nueva. Luego se puede editar las columnas (agregar / borrar / cambiar)y al finalizar solo se debe presionar el botón "Synchronize Column" (Sincronizar Columnas) para crear la tabla o añadir las columnas en la base de datos.

* Centrally Maintained (Mantenido Centralmente): Indica que tomará las traducciones que tenga definido en el Elemento.

## Columna

![image alt text](img/image_15.png)

**Descripción de los Campos:**

* System Element (Elemento del Sistema): Seleccionando el ELEMENTO se heredará por defecto el nombre de la columna en la base de datos, nombre, descripción y traducción del elemento. Si se está creando una columna nueva y no hay registro de un elemento similar al que se requiere crear entonces debe hacerse el registro del nuevo elemento haciendo ZOOM (Click derecho en el campo y seleccionar la opción Acercar).

* DB Column Name (Nombre de la Columna en BD): El nombre exacto de la columna en la base de datos.

* Column SQL (Columna SQL): Usado para crear columnas virtuales. Estas muestran información general, o información de otras tablas sin la necesidad de ser agregadas como columnas reales en la base de datos. Se construyen con una sentencia SQL de la tabla principal.

* Reference (Referencia): Tipo de dato de la columna. Cada referencia corresponde a un comportamiento diferente en la interfaz. Note cuidadosamente la diferencia en Table (Tabla) y Table Direct (Tabla Directa). Table Direct necesita tener una correspondencia exacta con la tabla a la que se está refiriendo. También debe notar la diferencia entre Table y Search (Búsqueda). Para los campos con tipo de referencia Button se puede definir un proceso asociado. Para las referencias tipo monto, fecha, entero, número, cantidad, se pueden definir rangos de mínimos y máximos.

* Validation (Validación Dinámica): Permite configurar cambios dinámicos en el campo.

* Reference Key (Llave de Referencia): Se utiliza para seleccionar listas estáticas definidas para la columna específica.

* Value Format (Formato del Valor): Para columnas con el tipo de referencia String se puede definir un formato específico. El formateo de ADempiere puede forzar el uso de espacio, cualquier letra, mayúscula, minúscula, letras y dígitos, sólo dígitos, entre otros. Por ejemplo: Definir el formato para números de teléfono.

* Default Logic (Lógica Predeterminada): Permite configurar de forma predeterminada; variables de Contexto, Sentencias SQL. para definir más de lógica estas deben ser separadas por ";"

* Key Column (Columna Clave): Sólo se puede definir una columna clave por tabla (Llave primaria) Normalmente es el ID, Este campo no es mostrado a los usuarios.

* Parent Link Column (Columna de Enlace a Tabla Padre): Define la relación hijo con una o más tablas (llave foránea). Pueden haber tablas sin ID principal pero con uno o más enlaces a tablas padre (como Tablas de Acceso).

* Mandatory (Obligatorio): Indica si el campo será obligatorio.

* Updateable (Actualizable): Indica si el campo será actualizable.

* Always updateable (Siempre Actualizable): Hace que el campo sea siempre actualizable aún después de haber sido procesado.

* Encryption (No Encriptado): Solo para campos de referencia tipo String. No tiene proceso de reversión. Se puede perder datos, se necesita asegurar el ancho de la columna para que pueda guardar todos los valores actuales.

* Read only logic (Lógica de Solo Lectura): Condición para que el campo sea de solo lectura. Por defecto los campo IsActive y Processed marcan el registro como de solo lectura sin necesidad de definir la lógica aquí.

* Mandatory logic (Lógica Obligatorio): Condición necesaria para que este campo sea obligatorio.

* Identifier (Identificador): Una o más columnas (normalmente value y/o name) que serán mostradas en listas y para referencias de reportes. Los identificadores son mostrados en el orden definido con la secuencia de los campos.

* Callout: Pedazo de código (customization) para llenar otros campos o para validaciones simples. No se recomienda usarlo para validaciones.

* Selection column (Columna de Selección): Define las columnas que serán mostradas en la ventana de búsqueda.

* Translated (Traducida): Para definir traducción para una columna. En este caso se necesitará crear una tabla y una pestaña con el mismo nombre que la original pero añadiendo el sufijo _Trl y crear la tabla con la misma llama de la tabla padre, columna para el lenguaje y columna para la traducción.

Finalmente las tablas pueden crearse:

* Desde la base de datos hacia el diccionario de aplicación. 

* Desde el diccionario de aplicación hacia la base de datos.

Para llevar un control de las tablas ADempiere maneja una serie de prefijos para la creación de tablas, mostrado a continuación.

<table>
  <tr>
    <td>Prefijo</td>
    <td>Ingles</td>
    <td>Español</td>
  </tr>
  <tr>
    <td>AD_</td>
    <td>Application Dictionary (e.: AD_Element)</td>
    <td>Diccionario de aplicación </td>
  </tr>
  <tr>
    <td>A_</td>
    <td>Assets Management (e.: A_Asset_Group)</td>
    <td>Gestión de Activos </td>
  </tr>
  <tr>
    <td>ASP</td>
    <td>Application Service Provider (e.: ASP_Module)</td>
    <td>Proveedor de servicios de aplicaciones</td>
  </tr>
  <tr>
    <td>B_</td>
    <td>Marketplace (e.: B_Buyer)</td>
    <td>Mercado</td>
  </tr>
  <tr>
    <td>C_</td>
    <td>Common or Core Functionality (e.: C_AcctSchema)</td>
    <td>Núcleo común o funcionalidad </td>
  </tr>
  <tr>
    <td>CM_</td>
    <td>Collaboration Management (e.: CM_WebProject)</td>
    <td>Gestión de Colaboración </td>
  </tr>
  <tr>
    <td>FACT_</td>
    <td>Multi-Dimensional Cube (e.: Fact_Acct)</td>
    <td>Cubo Multi-Dimensional</td>
  </tr>
  <tr>
    <td>GL_</td>
    <td>General Ledger (e.: GL_Journal)</td>
    <td>Contabilidad General </td>
  </tr>
  <tr>
    <td>HR_</td>
    <td>Human Resource (e.: HR_Payroll) </td>
    <td>Recursos Humanos </td>
  </tr>
  <tr>
    <td>I_</td>
    <td>Import (e.: I_BPartner)</td>
    <td>Importación </td>
  </tr>
  <tr>
    <td>K_</td>
    <td>Knowledge Management (e.: K_Category)</td>
    <td>Gestión del Conocimiento </td>
  </tr>
  <tr>
    <td>M_</td>
    <td>Material Management (e.: M_Cost)</td>
    <td>Gestión de materiales </td>
  </tr>
  <tr>
    <td>PA_</td>
    <td>Performance Analysis (e.: PA_Report)</td>
    <td>Análisis de rendimiento </td>
  </tr>
  <tr>
    <td>PP_</td>
    <td>Production Planning (e.: PP_Order)</td>
    <td>Planificación de la producción </td>
  </tr>
  <tr>
    <td>R_</td>
    <td>Requests (e.: R_Request)</td>
    <td>Las solicitudes </td>
  </tr>
  <tr>
    <td>RV_</td>
    <td>Report View (e.: RV_BPartner)</td>
    <td>Ver informe</td>
  </tr>
  <tr>
    <td>S_</td>
    <td>Service (e.: S_Resource)</td>
    <td>Servicio</td>
  </tr>
  <tr>
    <td>T_</td>
    <td>Temporary Tables (e.: T_Report)</td>
    <td>Tablas temporales </td>
  </tr>
  <tr>
    <td>W_</td>
    <td>Web (e.: W_Basket)</td>
    <td>Web </td>
  </tr>
  <tr>
    <td>WS_</td>
    <td>Servicio Web</td>
    <td>WS_WebService</td>
  </tr>
</table>


**Ejemplo:**

Para el siguiente ejemplo debe dirigirse en ADempiere en la sección de "Diccionario de Aplicación (Application Dictionary)" -> Tabla y Columna. Seguidamente se mostrará la Búsqueda de registros: Ventana de tablas donde debe hacer clic en el botón (Nuevo registro) en la esquina inferior izquierda.

 	En la siguiente imagen se muestra la creación de la tabla "RS_Table" basado en el modelo inicial.

![image alt text](img/image_16.png)

<table>
  <tr>
    <td>Campo</td>
    <td>Valor</td>
  </tr>
  <tr>
    <td>Nombre de la Tabla</td>
    <td>RS_Table</td>
  </tr>
  <tr>
    <td>Nombre</td>
    <td>Table</td>
  </tr>
  <tr>
    <td>Activo</td>
    <td>Si</td>
  </tr>
  <tr>
    <td>Vista</td>
    <td>No</td>
  </tr>
  <tr>
    <td>Nivel de Acceso a Datos</td>
    <td>Compañía+Organización</td>
  </tr>
  <tr>
    <td>Mantiene Bitácoras Cambio</td>
    <td>No</td>
  </tr>
  <tr>
    <td>Registros Eliminables</td>
    <td>Si</td>
  </tr>
  <tr>
    <td>Volumen Alto</td>
    <td>No</td>
  </tr>
  <tr>
    <td>Tipo de Entidad</td>
    <td>Curso</td>
  </tr>
  <tr>
    <td>Mantenido Centralmente</td>
    <td>No</td>
  </tr>
  <tr>
    <td>Auto Complete Min Length</td>
    <td>0</td>
  </tr>
  <tr>
    <td>IsDocument</td>
    <td>No</td>
  </tr>
</table>


Para agregar la traducción de la tabla es necesario ubicarse en la pestaña "Traducción" como se muestra en la siguiente imagen.

![image alt text](img/image_17.png)

<table>
  <tr>
    <td>Campo</td>
    <td>Valor</td>
  </tr>
  <tr>
    <td>Nombre</td>
    <td>Mesa</td>
  </tr>
  <tr>
    <td>Activo</td>
    <td>Si</td>
  </tr>
  <tr>
    <td>Traducida</td>
    <td>No</td>
  </tr>
</table>


Posteriormente se debe ubicar en la pestaña "Columna" y agregar los campos requeridos

* Name

* Value

![image alt text](img/image_18.png)

Una ventaja de crear columnas a partir de los elementos es que permite heredar los campos nombre de la columna en BD, nombre, descripción, ayuda, referencia y traducción del elemento mediante el campo Elemento del Sistema.

Primeramente como lo indica la imagen se registrará el campo "Name" con las siguientes características. 

![image alt text](img/image_19.png)

De igual manera, deberá realizar el mismo paso para el campo value. Al terminar de registrar todos las columnas requerida se debe hacer click en el botón "Sincronizar Columnas"

![image alt text](img/image_20.png)

Donde aparecerá el siguiente cuadro de diálogo, indica que ADempiere creará las columnas en la base de datos.

### ![image alt text](img/image_21.png)

Finalmente mostrará un mensaje donde indica el proceso realizado, en este caso el proceso fue de crear la tabla con sus respectivas columnas.![image alt text](img/image_22.png)Es necesario aplicar estos pasos para las tablas descritas en el caso práctico.

# Ventana, Pestaña y Campos

![image alt text](img/image_23.png)

La Ventana, Pestaña y Campo Ventana define la presentación de usuario (GUI) de tablas y columnas dentro de cada ventana. 

## Ventana

![image alt text](img/image_24.png)

**Descripción de Campos:**

* El nombre de la Ventana es utilizado como un identificador para acceso rápido.

* Funcionalidad beta, al marcar el check se considera como Beta y no es visualizada por el usuario final.

* El Tipo de ventanas permite seleccionar entre "Solo Consulta"; como su nombre lo indica solo es utilizada para la consulta de datos, “Transacción”; sólo aparecen los registros creado / modificado por última vez 24 horas o pendiente (el usuario puede seleccionar para mostrar más con el botón de  historial) y “Mantenimiento”; Las ventanas de este tipo son de características generales.

* Copiar Pestañas: Permite copiar pestañas desde otras ventanas ya creadas.

## Pestaña

![image alt text](img/image_25.png)

**Descripción de los Campos:**

* Tabla indica que la pestaña se basa en la tabla seleccionada para crear los campos.

* Crear Campos se utiliza para crear los campos de las columnas de la tabla en la pestaña.

**Ejemplo:**

Crear una nueva entrada en la ventana de "Ventana, Pestaña y Campo". con el nombre de "Table".![image alt text](img/image_26.png)

Se debe agregar su respectiva traducción desde la pestaña de "Traducción de Ventana".

![image alt text](img/image_27.png)

Seguidamente en la pestaña "Pestaña" crear una nueva entrada con el nombre de "Table" y en el campo "Tabla" seleccionar la tabla anteriormente creada "RS_Table".

![image alt text](img/image_28.png)

Utilice el botón "Crear campos" para crear campos para todas las columnas de la tabla.

![image alt text](img/image_29.png)

Al presionar el campo aparecerá un cuadro de diálogo se debe dar click en continuar.

![image alt text](img/image_30.png)

De igual manera se le debe crear la traducción a la pestaña, desde la pestaña "Traducción de Pestaña".

![image alt text](img/image_31.png)

En la Pestaña secuencia Campo se puede elegir el orden de los campos.

![image alt text](img/image_32.png)

Seguidamente se debe seleccionar la pestaña Campo donde se puede seleccionar múltiples atributos relacionados con la vista del sistema.

![image alt text](img/image_33.png)

Para ver el resultado de la ventana creada es necesario continuar con el siguiente capítulo.

# Menú

![image alt text](img/image_34.png)

En ADempiere la creación de Menú se puede hacer de manera rápida, es una ventana flexible donde se pueden crear los items de carpetas llamadas "Entidad Acumulada", ventanas, procesos, reportes formularios y más... para acceder a ella se debe ir al Menú | Administración del sistema | Reglas Generales | Sistema de Reglas | Menú.

Seguidamente haga clic en el botón Nuevo registro para crear un nuevo menú y rellenar los datos, como se muestra en la siguiente captura de pantalla:

![image alt text](img/image_35.png)

La imagen anterior nos muestra cómo agregar una carpeta para organizar las customizaciones realizadas. Ahora se debe crear un registro para mostrar la ventana previamente creada.

![image alt text](img/image_36.png)

Para añadir el ítem creado a la Entidad Acumulada (Carpeta) creada, basta solamente con arrastrar y soltar en la misma.

## Resultados:

Para ver los resultados deberá volver a iniciar sesión.

![image alt text](img/image_37.png)

Esta vez se debe seleccionar el rol de GardenWorld Admin como lo muestra la imagen

![image alt text](img/image_38.png)

De esta manera podrá ver en el menú los nuevos elementos creados.

![image alt text](img/image_39.png)

Seleccionar la ventana y aparecerá de la siguiente forma:

![image alt text](img/image_40.png)

En el siguiente capítulo se explicará cómo customizar la ventana.

# Personalizando Ventanas

## Diseño de Columnas

Teniendo en cuenta que ya tiene un conocimiento básico en la creación de ventana se procede a personalizar la ventana previamente creada. El diseño quedará como lo muestra la siguiente imagen.

![image alt text](img/image_41.png)

Para lograr este diseño deberá ingresar nuevamente como Administrador de Sistema y acceder en "Ventana, Pestaña y Campo".

![image alt text](img/image_42.png)

Seguidamente ubicarse en "Pestaña" y seleccionar la opción "Disposición de Reglón Simple" como lo muestra la imagen

![image alt text](img/image_43.png)

Cambiar la secuencia de los campos de la siguiente manera:

![image alt text](img/image_44.png)

Luego en la pestaña de "Campo" se cambiará lo siguiente:

<table>
  <tr>
    <td> Campo              </td>
    <td> Cambio               </td>
  </tr>
  <tr>
    <td> Compañía</td>
    <td>Marcar check "Solo lectura" </td>
  </tr>
  <tr>
    <td> Organización</td>
    <td>Marcar check "Solo Lectura" y "Misma Línea" </td>
  </tr>
  <tr>
    <td> Search Key</td>
    <td>Colocarle al Campo "Longitud de Despliegue 20"  </td>
  </tr>
  <tr>
    <td> Activo </td>
    <td>Marcar check "Misma Línea" </td>
  </tr>
  <tr>
    <td> Nombre</td>
    <td>Colocarle al Campo "Longitud de Despliegue 20"</td>
  </tr>
</table>


## Copiar Campos

Ahora se verá aspecto de personalización y ampliación de la capacidad de ADempiere. Se va a seguir utilizando la plantilla del caso de uso, que se ha mencionado en el comienzo, para mapear ADempiere de capacidad y demostramos cómo podemos extender su capacidad, haciendo uso de las construcciones como modelo, proceso y callout.

Como lo mencionado en el caso de uso deberá realizar lo siguiente:

Crear ventana para registrar los clientes (Solo la pestaña principal de socio de negocio y cliente, se debe predeterminar el campo cliente en verdadero). En este caso necesitará copiar los campos de las pestañas "Clientes" para ello se deberá crear sus respectivas pestañas.

Ahora, para crear rápidamente una copia de una pestaña existente, ADempiere proporciona un buen ahorro de tiempo a través de la característica que vamos a ver en esta sección.

Debe ingresar como  SystemAdmin con el rol de Administrador de Sistema.

Ir a "Ventana, Pestaña, Campo" y Crear un Nuevo registro llamado "Customer"(Cliente).

![image alt text](img/image_45.png)

 

Ahora debe crear una pestaña que será llamada "Customer"(Cliente), con las siguientes características:

![image alt text](img/image_46.png)

<table>
  <tr>
    <td> Campo    </td>
    <td> Valor    </td>
  </tr>
  <tr>
    <td> Nombre</td>
    <td> Customer</td>
  </tr>
  <tr>
    <td> Tabla</td>
    <td> C_BPartner</td>
  </tr>
  <tr>
    <td> Tipo de Entidad</td>
    <td> Curso</td>
  </tr>
</table>


 

 

Seguidamente debe presionar el botón "Copiar Pestañas", lo que le mostrará será un cuadro de diálogo donde en el campo "Pestaña" debe seleccionar "Customer" la cual será la pestaña que donde copiará los datos:

![image alt text](img/image_47.png)

Ahora se verifica los campos creados en la pestaña "Campos". Seleccionando el Campo "Customer" y comprobar que su lógica predeterminada es 'Y' como se muestra a continuación. Esto indicará que el campo "Cliente" siempre estará marcado cuando se cree un nuevo registro.

**Nota:**** **La lógica predeterminada es un concepto de ADempiere útil cuando se quiere asignar un valor predeterminado a un

campo. El valor predeterminado puede ser un valor estático o puede ser un valor derivado. 

![image alt text](img/image_48.png)

Ahora deberá agregar en el menú con el nombre "Customer" como se ha explicado en el capítulo anterior..

**_Nota:_** *Debe recordar crear las respectivas traducciones para las ventanas, pestañas e ítems del menú que ha creado.*

### **El Resultado es el Siguiente:**

![image alt text](img/image_49.png)

## Lógica Sólo Lectura

La lógica de solo lectura es utilizado para editar el comportamiento de un campo. Según el caso práctico indica que debe:

Modificar el campo "Cerrado" de la ventana  "Asignación de Mesa", el cual debe tener una lógica de solo lectura cuando esté en verdadero.

Para realizar este paso necesita ir a "Tabla y Columna" ubicar la tabla previamente creada "RS_Table_Allocation".

![image alt text](img/image_50.png)

En la pestaña "Campo" debe seleccionar el campo "IsClosed" y modificar el campo "Lógica de Solo Lectura" como lo muestra la imagen:

![image alt text](img/image_51.png)

Con este cambio ADempiere evalúa la expresión mencionada en este campo en el momento de mostrar un registro. Si la expresión se evalúa como verdadera, el campo se convierte en sólo lectura. De lo contrario, sigue siendo editable. Usted puede construir las expresiones usando las variables de contexto predefinidas en ADempiere.

# Reportes

La ventana "Reportes y Procesos" es usada para definir los parámetros y reglas de acceso para cada reporte y proceso corriendo en el sistema. Esta ventana solamente puede ser accedida por el rol System Administrator. 

La ventana para el registro de los Informe y procesos en el sistema se encuentra ubicada en Menu » Diccionario de Aplicación » Informe y Procesos (Report & Process)

![image alt text](img/image_52.png)

Esta ventana está compuesta por 4 pestañas (La principal y las dependientes):

Informe y Proceso: Donde se registra el código y nombre que identifica al reporte o al proceso en el sistema y la clase, procedimiento o flujo de trabajo que contiene las funciones manejadoras del reporte o proceso.

1. Traducción de Informe y Proceso: Donde se coloca la traducción al otro idioma del nombre del proceso (el nombre que mostrará en el menú).

2. Acceso a Informe y Proceso: Donde se indica el rol que tendrá acceso al reporte o proceso. Por defecto está registrado System Administrator.

3. Parámetro: Donde se definen los parámetros para el manejo de los datos en el reporte o proceso.

    1. Traducción de Parámetro

![image alt text](img/image_53.png)

Para registrar un reporte en ADempiere se deben colocar principalmente los siguientes datos:

* Código: Para que el sistema no coloque la secuencia de código por defecto para el reporte, de modo que puedas encontrarlo fácilmente.

* Nombre

* Tipo de Entidad: La entidad con que se está  identificando los datos agregados al sistema por tu customización.

* Nivel de Acceso de Datos: Asegúrate de que esté seleccionado Sistema+Empresa

* Nombre de clase: La clase manejadora de los reportes es org.compiere.report.ReportStarter

* Si estás trabajando con iReport (el cual es mi caso) entonces colocas el nombre del archivo jrxml donde diseñaste el reporte, esto es en el campo Reporte Jasper. Para evitar problemas al levantar el reporte entonces adjuntas el archivo jrxml desde la opción adjuntar que tiene la ventana, quedando de esta forma el dato en el campo Reporte Jasper:

[attachment](http://www.adempiere.com/Attachment):myReport.jrxml.

* Si los datos del reporte están siendo filtrados por unas variables específicas debes registrar esas variables en la pestaña parámetros. Seleccionando el nombre del campo en la base de datos y en el campo Elemento del Sistema se debe colocar el nombre igual al usado en la variable del reporte (en ireport).

* Una vez registrado el reporte deberá agregarlo en el menú para que así pueda acceder a él.

# Formato de Impresión

Mediante la manipulación de los formatos de impresión se pueden cambiar los documentos que genera Adempiere, para que cumplan con las normas exigidas por la ley y para hacerlos parecidos a los formatos que se manejan en las empresas.

          ![image alt text](img/image_54.png)

Para acceder a los formatos disponible lo hacemos por Administración del Sistema → Reglas Generales → Impresión → **Formato de Impresión**.

           ![image alt text](img/image_55.png)

Las pestañas "Orden de Despliegue"  como sus nombre indica sirve para cambiar en qué orden aparecen los campos a mostrar.        ![image alt text](img/image_56.png)

Su utilización simplemente consiste en pasar los campos de la izquierda a la derecha.

La pestaña "Criterio de Ordenamiento" se definen las columnas por las que se desean ordenar las filas de los reportes.

![image alt text](img/image_57.png)

En la pestaña "Elemento de Formato" se definen cada uno de los campos que hacen parte del formato.             ![image alt text](img/image_58.png)

Si lo vemos en forma de tabla nos muestra los campos actualmente definidos, para modificarlos solo debemos seleccionar el campo y lo mostramos en vista de formulario

            ![image alt text](img/image_59.png) 

Manipulando estos campos podemos modificar el formato, por ejemplo puede cambiar el logo que aparece por defecto y poner el logo de su compañía.

Los siguientes son los campos que debemos llenar, algunos de estos se activan o se desactivan dependiendo del "Tipo de Formato"

<table>
  <tr>
    <td>Nombre</td>
    <td>Descripción</td>
  </tr>
  <tr>
    <td>Secuencia</td>
    <td>Indica el orden de los registros</td>
  </tr>
  <tr>
    <td>Nombre</td>
    <td>Se utiliza como referencia para hacer busqueda</td>
  </tr>
  <tr>
    <td>Nombe a ser Impreso</td>
    <td>El nombre a ser impreso indica el nombre que será impreso en el documento</td>
  </tr>
  <tr>
    <td>Impresión Etiqueta Subfijo</td>
    <td>indica el nombre que será impreso después del campo.</td>
  </tr>
  <tr>
    <td>Mantenido Centralmente</td>
    <td>Se utiliza para mantener la traducción del elemento.</td>
  </tr>
  <tr>
    <td>Suprimir Nulos</td>
    <td>Si el elemento de la columna a imprimir es nulo; el suprimir nulo causa que el campo nulo y su título no sean impreso.</td>
  </tr>
  <tr>
    <td>Tipo de Formato</td>
    <td>Es una lista de selección para indicar el tipo de formato que será impreso</td>
  </tr>
  <tr>
    <td>Columna</td>
    <td>Se utiliza para indicar la columna de la tabla</td>
  </tr>
  <tr>
    <td>Anchura de la línea</td>
    <td>Indica el ancho físico de las líneas</td>
  </tr>
  <tr>
    <td>Formato de Impresión incluido</td>
    <td>Permite escribir las líneas de detalles del documento</td>
  </tr>
  <tr>
    <td>Archivo de Imagen</td>
    <td>Check para indicar la columna de la base de datos con la url de la imagen a mostrar</td>
  </tr>
  <tr>
    <td>Imagen Adjunta</td>
    <td>Check para indicar que la imagen está adjunta en el registro</td>
  </tr>
  <tr>
    <td>Url de la imagen</td>
    <td>indica url externa a la base de datos</td>
  </tr>
  <tr>
    <td>Área</td>
    <td>Indica el área de impresión para este ítem</td>
  </tr>
  <tr>
    <td>Posición Relativa</td>
    <td>La posición relativa del ítem es determinado por el espacio X-Z y la próxima línea</td>
  </tr>
  <tr>
    <td>Fijar Posición NL</td>
    <td>Si es habilitada la posición actual X (horizontal) antes de imprimir el ítem es guardada. La próxima línea usará esta posición X; Permitiendo la impresión por columnas. </td>
  </tr>
  <tr>
    <td>Posición X</td>
    <td>Posición absoluta  X en 1/72 pulgada</td>
  </tr>
  <tr>
    <td>Posición Y</td>
    <td>Posición absoluta  Y en 1/72 pulgada</td>
  </tr>
  <tr>
    <td>Proxima Linea</td>
    <td>Si no se marca el ítem es impreso en la misma línea</td>
  </tr>
  <tr>
    <td>Próxima Página</td>
    <td>Antes de imprimir esta columna; habrá un cambio de página</td>
  </tr>
  <tr>
    <td>Alineación de línea</td>
    <td>Es usado para posicionamiento relativo del texto (centro, derecha o izquierda)</td>
  </tr>
  <tr>
    <td>Alineación del Campo</td>
    <td>Indica la alineación de los campos, el valor por defecto depende del tipo de dato a mostrar</td>
  </tr>
  <tr>
    <td>Espacio X</td>
    <td>Espacio Relativo  X en 1/72 de pulgada en relación al ítem anterior</td>
  </tr>
  <tr>
    <td>Espacio Y</td>
    <td>Espacio Relativo  Y en 1/72 de pulgada en relación al ítem anterior</td>
  </tr>
  <tr>
    <td>Máximo Ancho</td>
    <td>Máximo ancho del elemento medido en 1/72 de pulgada. Si se indica 0; no hay restricción</td>
  </tr>
  <tr>
    <td>Ancho Fijo</td>
    <td>Indica ancho fijo de la columna independientemente del contenido</td>
  </tr>
  <tr>
    <td>Tipo de Formato</td>
    <td>Se puede seleccionar como campo, linea, rectangulo, texto fijo o una imagen</td>
  </tr>
  <tr>
    <td>Máxima Altura</td>
    <td>Máximo Altura del elemento medido en 1/72 de pulgada. Si se indica 0; no hay restricción</td>
  </tr>
  <tr>
    <td>Una Línea</td>
    <td>Si la columna tiene restricción de ancho; el texto es dividido en líneas múltiples. Si una línea es seleccionado; solamente la primera es impresa.</td>
  </tr>
  <tr>
    <td>Impresión a Color</td>
    <td>Color usado para imprimir</td>
  </tr>
  <tr>
    <td>Fuente de Impresión</td>
    <td>Tipo de Fuente usado para imprimir</td>
  </tr>
  <tr>
    <td>Ordenado por</td>
    <td>Los registros son ordenados por el valor de esta columna</td>
  </tr>
  <tr>
    <td>Calcular Sumatoria</td>
    <td>Calcula la suma total de los datos si el campo es numérico; de otra manera calcula la longitud total del campo.</td>
  </tr>
  <tr>
    <td>Calcular Conteo</td>
    <td>Calcula el Número total de elementos no vacíos;</td>
  </tr>
  <tr>
    <td>Calcular Mínimo</td>
    <td>Calcula el valor mínimo de los campos</td>
  </tr>
  <tr>
    <td>Calcular Máximo</td>
    <td>Calcula el valor máximo de los campos</td>
  </tr>
  <tr>
    <td>Calcular Promedio</td>
    <td>Calcula el promedio de los datos.</td>
  </tr>
  <tr>
    <td>Calcular Desviación</td>
    <td>La desviación estándar es una medida de dispersión</td>
  </tr>
  <tr>
    <td>Calcular Variación</td>
    <td>La variación es una medida de dispersión</td>
  </tr>
</table>


Si hay campos que no están en el formato de impresión también se puede agregar otros campos para colocar elementos que no estén y que sean considerados necesarios.

Ej. Agregar a la Orden la fecha de impresión:

Primero se debe elegir el formato de impresión que desea modificar: En este caso para las órdenes es Order_Header. ![image alt text](img/image_60.png)

Seguidamente se debe ubicar en la pestaña "Elemento de Formato" y hacer clic en el botón nuevo, para agregar el nuevo elemento.

![image alt text](img/image_61.png)

Podemos observar todos los campos disponibles, primero debe indicar el nombre, en este caso "Fecha del Documento". ![image alt text](img/image_62.png)

En el campo "Nombre a ser Impreso" colocamos lo que deseamos que salga escrito, pero como la fecha no es algo fijo deberá utilizar una variable.

Las variables que se pueden utilizar son:

@*PageCount@ - Total de páginas

@*MultiPageInfo@ - "Página x de y"

@*CopyInfo@ - Si es una copia se imprime la palabra "Duplicado"

@*ReportName@ - Nombre del reporte

@*Header@ - Se imprime el encabezado total, Nombre de la compañía y de La organización

@*CurrentDate@ - La fecha actual

@*CurrentDateTime@ - La fecha y hora actual

También puede colocar texto además de la variable como muestra la siguiente imagen.             ![image alt text](img/image_63.png)

Podrá notar que en "Tipo de Formato" está seleccionado “Campo”, esto le permite seleccionar la columna de la consulta que desea imprimir, pero como la fecha no hace parte de la columna sino del sistema, entonces debe cambiar el “Tipo de Formato” a “Texto”.

![image alt text](img/image_64.png)

En "Área" se indica en qué parte del formato desea que salga el nuevo elemento, en este caso se seleccionará que esté en el “Pie de Página”

![image alt text](img/image_65.png)

Finalmente podrá observar el nuevo elemento entrando a la ventana "Órdenes de Compras" y Previsualizar el documento mediante el botón “Vista Preliminar”.

Instalar y Configurar Eclipse en GNU/Linux

# Instalación de Eclipse

Accedemos a la página web oficial de Eclipse y descargamos la versión disponible del entorno de desarrollo para Java.

**Nota: **En este caso trabajaremos con la versión Luna.

[Eclipse Luna](https://eclipse.org/downloads/packages/eclipse-ide-java-developers/lunasr2)

Extraemos el contenido del archivo comprimido en la carpeta de trabajo utilizada:

unzip[ eclipse-java-luna-SR2-win32-x86_*.zip](http://www.eclipse.org/downloads/download.php?file=/technology/epp/downloads/release/luna/SR2/eclipse-java-luna-SR2-win32-x86_64.zip&mirror_id=576)  -d /opt/Apps

Luego debe abrir el explorador de archivo y ubicar la carpeta de instalación del Eclipse:

/opt/Apps/eclipse

Para ejecutar el eclipse se le debe dar click al archivo "eclipse".

# Creando el Entorno de Desarrollo ADempiere

Con este objetivo en perspectiva, vamos a entender cómo configurar el entorno de desarrollo ADempiere en Eclipse y los distintos mecanismo de despliegue que se pueden aplicar al aspecto operacional de ADempiere.

La creación del entorno de desarrollo es el primer paso para convertirse en un desarrollador con ADempiere. El entorno de desarrollo le permitirá probar y depurar la aplicación y hacer cambios a la comunidad. Para probar la interfaz de usuario Web (WebUI) tendrá que seguir los pasos en la creación del espacio de trabajo Web UI usando Eclipse Webtool después de crear el entorno de desarrollo. También, se recomienda que se cree un proyecto separado para el trabajo de personalización que no será compartido con todos los usuarios de Adempiere.  

## Preparación del entorno

Abrir el programa "Eclipse":

![image alt text](img/image_66.png)

Seleccionar espacio de trabajo

Nota: En esta ocasión se creará una carpeta denominada "Curso", ubicada en /opt/Development/ 

#### ![image alt text](img/image_67.png)

Para crear un nuevo proyecto, haga click en el menú  Eclipse File -> New -> Project

#### ![image alt text](img/image_68.png)

En el siguiente cuadro de diálogo, seleccione "Java Project" y "Next"

#### ![image alt text](img/image_69.png)

Desactive la casilla de verificación "Use default location" y establezca la ubicación para señalar en su ubicación de repositorio de trabajo (por ejemplo /opt/development/Curso\adempiere_xxx). El asistente creará automáticamente el proyecto del fuente existente. Haga clic en "Next" y luego en "Finish".

#### ![image alt text](img/image_70.png)

## Compilando ADempiere

Para compilar el proyecto y ejecutarlo se debe seleccionar el siguiente icono ![image alt text](img/image_71.png)  y escoger la opción "Run Configuration", como se muestra en la imagen:

#### ![image alt text](img/image_72.png)

Se desplegará la siguiente ventana y deberá presionar![image alt text](img/image_73.png) el icono para crear una nueva configuración.

#### ![image alt text](img/image_74.png)

Seguidamente debe llenar los siguientes campos:

<table>
  <tr>
    <td>Campos</td>
    <td>Descripción</td>
    <td>  Ejemplo</td>
  </tr>
  <tr>
    <td> Name</td>
    <td> Nombre de la configuración</td>
    <td>  adempiere</td>
  </tr>
  <tr>
    <td> Project</td>
    <td> Nombre del Proyecto a Ejecutar</td>
    <td>  adempiere_380</td>
  </tr>
  <tr>
    <td> Main Class</td>
    <td> Clase Principal del Proyecto</td>
    <td>  org.compiere.Adempiere</td>
  </tr>
</table>


#### ![image alt text](img/image_75.png)

Luego debe seleccionar la pestaña "Arguments", ésta acción abre una ventana donde se modificará el campo de "VM Arguments". Debe agregar lo siguiente: 

-DPropertyFile=/opt/Development/ADempiere_Properties/ADempiere_XXX.properties

-DADEMPIERE_HOME=/Applications/Adempiere-Xms512M-Xmx512M

-Dorg.adempiere.server.embedded=true

**Nota:**** **Los Argumentos de VM (Virtual Machine) son típicamente valores que cambian el comportamiento de la máquina virtual de Java (JVM). Por ejemplo, el argumento -Xmx512m permite que la pila de Java pueda crecer hasta 512 MB.![image alt text](img/image_76.png)

# Finalmente ya está configurado Adempiere y se le debe dar clic al botón "Apply" seguidamente del botón "RUN" para ejecutar el proyecto, de esta manera se desplegará la ventana de carga de ADempiere.

# Clases Modelo

Existen varias opciones en ADempiere para darle vida al modelo diseñado con el Diccionario de Datos. Para esto se debe entrar al código, con un poco de conocimiento en Java y en la funcionalidad de ADempiere podrá ser capaz de hacer desarrollos y personalizaciones del sistema por su propia cuenta mejorando la herramienta.

ADempiere contiene una clase llamada GenerateModel.java ubicado en el paquete org.adempiere.util.

Esta clase sirve para extender la funcionalidad de las tablas que creemos en el sistema. Con ella podemos establecer la *persistencia de objeto* para nuestras tablas con el fin de acceder a sus datos para Crear, Leer, Actualizar y Borrar. El modelo esta representado por las clases X_"NombreDeTabla" y las clases I_"NombreDeTabla", las cuales estan compuestas por los metodos getter y setter que nos permitiran manejar los datos de la tabla por toda la aplicacion.

Las clases X_"NombreDeTabla" extienden de la clase PO (Persistent Object) e implementan las clases (interfaces) I_"NombreDeTabla" correspondiente y la clase I_Persistent

**Nota:**** ***En programación, entendemos por Persistencia de Objeto (PO) a: "La acción de preservar la información de un objeto de forma permanente, y a su vez poder recuperar la información del mismo para ser utilizada nuevamente en diferentes propósitos"*

## Antes de Empezar...

Debe crear un proyecto java, esto es con la finalidad de no modificar el fuente base de ADempiere, además es considerada una buena práctica a la hora de hacer customizaciones de proyectos.

1. Debe ubicarse en el menú de eclipse, la opción File -> New -> Project...

![image alt text](img/image_77.png)

2. En el cuadro de diálogo desplegado deberá seleccionar la opción "Java Project". Y Presionar el botón "Next"

![image alt text](img/image_78.png)

3. Seguidamente colocarle el nombre al proyecto. Para este ejemplo se colocará "ADempiere-Basic" y presionar el botón "Finish".

![image alt text](img/image_79.png)

Una vez creado el proyecto debe modificar sus propiedades e indicarle que depende del proyecto de "ADempiere_380", debe ubicarse en el menú de eclipse en la siguiente opción: Project -> Properties

![image alt text](img/image_80.png)

Luego ingresar en la opción "Java Build Path" y presionar el botón agregar.

![image alt text](img/image_81.png)

En el cuadro de diálogo desplegado se debe seleccionar el proyecto "adempiere_380" y presionar el botón "Ok" como se muestra a continuación.

![image alt text](img/image_82.png)

Finalmente se utilizará este proyecto para ejecutar ADempiere.

## Generar Clases Modelo

#### **Pasos a seguir para generar las Clases Modelo:**

1. Desde el entorno de desarrollo (eclipse), ir a la opción para correr o ejecutar el programa "RUN" y seleccionar la opción "RUN CONFIGURATIONS"

2. Crear una configuración para Generar las Clases.

![image alt text](img/image_83.png)

3. Ir a la pestaña "Arguments" para indicar los argumentos validos para generar el modelo.

![image alt text](img/image_84.png)

4. En el campo "Program Arguments" se ha agregado lo siguiente:

**"/opt/Develoment/Curso/ADempiere-Basic/base/org/curso/model/"**

**"org.curso.model"**

**"'ERP_Curso'"**

La primera linea indica la carpeta donde estarán ubicadas las clases, la siguiente indica el paquete de las clases y por ultimo se agrega el Tipo de Entidad.

Además en el campo VM arguments fue agregado, para obtener la configuración del proyecto anterior:

**-DPropertyFile=/opt/Development/ADempiere_Properties/ADempiere.properties**

**-DADEMPIERE_HOME=/opt/Apps/Adempiere**

**-Xms512M**

**-Xmx512M**

**-Dorg.adempiere.server.embedded=true**

5. Finalmente se guardan los cambios y se presiona el botón "RUN". 

En la consola de eclipse mostrará una serie de mensajes relacionado con la creación de las Clases Generadas. Siguiendo el caso practico la consola de eclipse debe mostrar que ha generado dos clases: 

**GenerateModel.main: Generated = 2.**

Esto es lo principal que debemos saber para trabajar con el código de ADempiere.

**_Nota:_*** Cada vez que agregamos un nuevo campo a cualquier tabla y vamos a necesitar manejar los datos mediante un validador, un callout o un proceso se debe generar el modelo para esa tabla que se ha modificado (En este caso actualiza las clases X_, I_ que habíamos creado previamente).*

En ADempiere una vez que hemos generado las clases X_"NombreDeTabla" podemos crear las clases M"NombreDeTabla" (las manejadoras o "Clase Manager"), puesto que estas extienden de las clases X_"NombreDeTabla".  Estas 'clases M' son clases abstractas que definen métodos como load(), save(), delete(), get_ID(), entre otros, que los podemos invocar desde nuestro código personal donde hayamos instanciado esa clase.

Estas clases manager se usan de dos formas:

    1. **Master Data Object Classes:** Extienden de la clase X_"NombreDeTabla" y permiten manejar los datos maestros de una tabla determinada. Ej. *MProduct extends X_M_Product*

    2. **Workflow Business Object:** Estas permiten manejar los objetos de negocio usados para manejar los flujos de trabajo de un proceso determinado. Aparte de extender de la clase X_"NombreDeTabla" implementan la interfaz 'DocAction'. Ej. *MInvoice extends X_C_Invoice implements DocAction*.

Estas son las clases que nos permiten ir más allá de la funcionalidad básica de ADempiere. En estas clases podemos definir métodos o funciones que manejaran los datos de una manera específica en el sistema. 

**Conceptualización del MVC en ADempiere:**

El código fuente de ADempiere está diseñado bajo el patrón MVC significa patrón Modelo-Vista-Controlador. Este patrón separa los datos y la lógica de negocio de una aplicación de la interfaz de usuario y el módulo encargado de gestionar los eventos y las comunicaciones. Esto permite reutilizar la parte lógica del código para utilizarla en diferentes vistas (interfaces). Aprovechando esa ventaja ADempiere maneja dos interfaces, una es la interfaz de escritorio desarrollada en Java y otra interfaz web basada en el Zk framework para  aplicaciones web en AJAX.

**Modelo**: Representa un objeto o Java que transporta datos.  Envía a la 'vista' aquella parte de la información que en cada momento se le solicita para que sea mostrada (típicamente a un usuario). Las peticiones de acceso o manipulación de información llegan al 'modelo' a través del 'controlador'.

En ADempiere las clase modelo se extiende de la clase PO estas se definen X_"NombreTabla” que a la vez implementa una interfaz definida como I_”NombreTabla”, como se observó anteriormente estas clases son generadas desde adempiere, Para utilizarlas se debe crear la Clase Manager.

La clase PO ofrece varios métodos que se pueden sobrescribir. Estos son afterSave, beforeSave, afterDelete, y beforeDelete. 

La sintaxis de estos métodos son:

**_protected_** **_boolean_** afterSave (**_boolean_** newRecord, **_boolean_** success) 

**_protected_** **_boolean_** afterDelete (**_boolean_** success) 

**_protected_** **_boolean_** beforeSave (**_boolean_** newRecord) 

**_protected_** **_boolean_** beforeDelete ()

El uso principal de estos métodos es para realizar tareas adicionales en un registro particular de una tabla mientras se guarda o se elimina dicho registro.

**Vista:** representa la visualización de los datos que contiene el modelo en un formato adecuado para interactuar con el usuario.

 

Un ejemplo en ADempiere son:

* **Window (Ventana): Orden de Venta**

![image alt text](img/image_85.png)

* ** Form (Formulario): Selección de Pago Manual**

![image alt text](img/image_86.png)

* **Smart Browser (Ventana Inteligente): Generar Movimientos**

![image alt text](img/image_87.png)

** Work Flow (Flujo de Trabajo): Configuración de Producto**

![image alt text](img/image_88.png)

**Controlador:** actúa tanto en modelo y la vista. Controla el flujo de datos en un objeto de modelo y actualiza la vista siempre que cambien los datos. Mantiene la vista y el modelo separado.** **En adempiere el controlador pueden ser definidos como: Callouts, advertencia de salvado y/o errores.

**Estructura de los directorios:**

**base: **En este directorio son incluidas las clases modelo, controladores y clases de utilidades que pueden ser usadas para las diferentes interfaces.

**client: **En este directorio se encuentra las clases de la vista de la interfaz de escritorio.

**zkwebui/WEB-INF/src: **al igual que **client **mantiene las clases de la interfaz web.

Cada directorio o carpeta de codigo esta compuesta por paquetes, los directorios de **Client/src y zkwebui/WEB-INF/src **mantienen estructura similar a nivel de paquete y clase.

**Nomenclatura para Paquetes:**

 Cada paquete comienza con **org.nombreentidad.**

Para identificar entre formulario, proceso y reporte se utiliza lo siguiente

	org**.**nombreentidad.**form**

	org**.**nombreentidad.**process**

	org**.**nombreentidad.**report**

# Procesos

Un proceso en ADempiere es una clase que se ejecuta cuando el usuario pulsa un botón en la interfaz gráfica. La clase debe extender de SvrProcess la cual contiene los métodos que regulan el funcionamiento de los procesos.

**_public class_*** CloseTable ***_extends_*** SvrProcess { ... }*

Los principales métodos son:

* protected void prepare() {} - Con éste método se captura el valor de los parámetros. Los parámetros que va a recibir el proceso se configura en la pestaña** Parámetros** de la ventana Reportes & Procesos.

* protected String doIt() throws Exception {} - Donde se define cómo se manejarán los datos (Se realiza la programación específica de ese proceso).

* protected void postProcess(boolean success) - Cuando el sistema entra a este 

* método ya la transacción ha sido completada y no se puede dar vuelta atrás.

* Este método es opcional, lo aplicas si quieres realizar alguna acción en caso

* de que el proceso haya sido exitoso o no.

Para la creación de procesos se utiliza la ventana de Informe y proceso, al igual que para un informe se deben llenar los campos "Nombre", “Tipo de Entidad”, “Nivel de Acceso a Datos” a diferencia que se debe colocar el “Nombre de Clase”.

En este ejemplo se debe colocar:

![image alt text](img/image_89.png)

	**Código**: Close Table

	**Nombre**: Close Tabe

	**Tipo de Entidad:** Curso

	**Nivel de Acceso a Datos**: Compañia + Organización

	**Nombre de Clase**: org.curso.process.CloseTable

En el eclipse se procede a crear la clase CloseTable.java en el paquete org.curso.process

![image alt text](img/image_90.png)

**_public class_*** CloseTable ***_extends_*** SvrProcess {*

*	// Parameter value*

**private** int p_AD_User_ID = 0;

	

	**public** CloseTable() {

	}

	@Override

	**protected**** **void prepare() {

			ProcessInfoParameter[] parameter = getParameter();

			for(int i=0; i < parameter.length; i++) {

				String name = parameter[i].getParameterName();

				if(name == **null**)

					;

				else if(name.equals("AD_User_ID")) {

					p_AD_User_ID = parameter[i].getParameterAsInt();

				}

			}

	}

	@Override

	**protected** String doIt() throws Exception {

		

		List<MRSTableAllocation> tableAllocations = MRSTableAllocation.

				getByUser(getCtx(), p_AD_User_ID, get_TrxName());

		int qty = 0;

		for(int i = 0; i < tableAllocations.size(); i++){

			if(!tableAllocations.get(i).isClosed()) {

				tableAllocations.get(i).setIsClosed(true);

				tableAllocations.get(i).saveEx();

				qty++;

			}

				

		}

			

		**return** Msg.getMsg(getCtx(), "Table Close ")+qty;

	}

En nuestra clase modelo **MRSTableAllocation **se debe crear el siguiente método:

**public** static List<MRSTableAllocation> getByUser(Properties ctx,

			int AD_User_ID, String trxName) {

		**final** String whereClause = "AD_User_ID=?";

				

		Query q = new Query(ctx, "RS_Table_Allocation", 

						whereClause, trxName);

				

		q.setParameters(AD_User_ID);

				

		return (q.list());

	}

 


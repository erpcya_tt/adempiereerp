Guía Práctica de ADempiere

# Contenido

[[TOC]]

**ADempiere ERP**

# **Sobre ADempiere**

ADempiere ERP es una sofisticada solución de negocios Open Source actualmente posicionada como una fuerte alternativa a los software propietarios, es decir, a aquellas aplicaciones que son desarrolladas y distribuidas por una organización comercial.

**Sus fortalezas:**

* Flexibilidad

* Viabilidad a largo Plazo

* Bajo Costo de Propiedad.

* Multiplataforma (Linux, WIndows, Mac).

**Características:**

* **Múltiples organizaciones dentro de la empresa**: ADempiere puede tener varias sucursales y empresas en una sola instalación, lo que es especialmente atractivo en grandes franquicias donde la consolidación de los datos financieros y operacionales es crítica.

* **Múltiples Idiomas toda la empresa y sus clientes**: Todo usuario y la información de cara al cliente se puede presentar en varios idiomas. Esto, junto con organizaciones múltiples, hace de Adempiere especialmente atractiva para las empresas multinacionales.

* **Formatos de Contabilidad múltiples**: Los datos contables pueden ser gestionados y presentan múltiples esquemas contables de la solicitud de ADempiere especialmente adecuado para el entorno multinacional.

* **Múltiples Sistemas Operativos**: Después de haber sido desarrollada en Java, Adempiere es capaz de funcionar en la mayoría de los sistemas operativos.

**Instalación y Configuración ADempiere**

# **Requisitos de Instalación**

1. [PostgreSQL versión 9.](http://www.enterprisedb.com/products-services-training/pgdownload)[x.x.](http://www.enterprisedb.com/products-services-training/pgdownload)

2. [JDK 1.](http://www.oracle.com/technetwork/java/javase/downloads/index.html)[x.x.](http://www.oracle.com/technetwork/java/javase/downloads/index.html)

3. [ADempiere 3.x.](https://github.com/adempiere/adempiere)

# Instalación de PostgreSQL

Una vez descargado el instalador de PostgreSQL 9.x, ejecute como usuario root.

Al presionar el botón "Siguiente" aparecerá una ventana donde se debe configurar la ruta de instalación del postgreSQL.

Luego deberá escribir una contraseña para el usuario root de PostgreSQL.

A continuación deberá colocar el puerto donde escuchará el servidor de PostgreSQL

En la ventana siguiente deberá seleccionar la configuración regional.

Luego, se copiarán automáticamente los archivos necesarios para que PostgreSQL pueda funcionar correctamente en su equipo.

Luego, terminará la instalación.

# **Instalación de JDK**

1. Una vez descargado el JDK 1.X.x, abra el terminal y ubique el directorio donde se encuentra el comprimido.

cd /opt/Install

**Nota: **En este caso se colocará en "/opt/Install" quedando así: “/opt/Install/jdk-Xx-linux-x64.tar.gz”.

2. Luego descomprima el archivo jdk-Xx-linux-x64.tar.gz:

tar xzvf jdk-7x-linux-x64.tar.gz

3. Ejecute el binario generado:

./jdk-7x-linux-x64.bin

4. A continuación copie el directorio creado al momento de ejecutar el binario en la ubicación /usr/local/:

cp -r jdk /usr/local

5. Deberá otorgar permisos de ejecución al directorio copiado en /usr/local:

chmod +x -R /usr/local

6. Ahora deberá agregar el java a las alternativas:

update-alternatives --install /usr/bin/java java /usr/local/jdk/bin/java 1

7. Modifique el archivo "~/.bashrc"

nano ~/.bashrc

Agregue las siguientes líneas al final del archivo:

JAVA_HOME="/usr/local/jdk"

PATH="$PATH:/usr/local/jdk/bin"

export JAVA_HOME

export PATH

Para finalizar, reinicie el equipo.

# **Instalación de ADempiere Serve****r**

Ejecute el PostgresSQL e indique la contraseña para el usuario "postgres", previamente configurado. 

Cree un usuario con el nombre adempiere en el servidor de PostgreSQL:

En los Privilegios deberá seleccionar "Puede Crear Base de Datos":

A continuación cree una base de datos bajo el usuario adempiere, en este ejemplo se denominará "adempiere":

# **Instalación de ADempiere**

Abra el siguiente enlace desde su navegador:

[http://www.adempiere.net/web/guest/download](http://www.adempiere.net/web/guest/download)

En la sección de download debe seleccionar la opción "Adempiere_XXXLTS.tar.gz"

![image alt text](img/image_0.png)

Una vez descargado ADempiere, ubique el directorio donde fue guardado.

cd /opt/Install

Nota: En este caso se colocará en "/opt/Install" quedando de la siguiente manera “/opt/Install/ Adempiere_XXXLTS.tar.gz”.

Descomprima el archivo:

tar xzvf Adempiere_380LTS.tar.gz -C /opt/Apps/

Restaure el backup que se encuentra en el directorio "/opt/Apps/Adempiere/data/", utilizando el siguiente comando:

/opt/Apps/PostgreSQL/9.3/bin/psql -U adempiere -W -h localhost -d adempiere < /opt/Apps/Adempiere/data/Adempiere_pg.dmp

Vaya al directorio raíz de Adempiere:

cd /opt/Apps/Adempiere

Ejecute el archivo RUN_setup.sh:

./RUN_setup.sh

Aparecerá la siguiente ventana:

![image alt text](img/image_1.png)

Establezca los valores del servidor de aplicación de ADempiere, los parámetros del servidor de PostgreSQL y del correo, si lo requiere. Luego, presione el botón "Probar" (El sistema comprobará los valores).

Si todo está correcto mostrará el siguiente diálogo que deberá confirmar. 

Presione el botón "Guardar" para comenzar la instalación, el sistema le pedirá los datos para crear la huella digital.

A continuación se le pedirá que acepte los términos de la licencia:

Aparecerá el siguiente mensaje, indicándole que fue guardada su configuración:

Presione el botón "Aceptar" y espere que termine la instalación, la ventana de configuración se cerrará automáticamente.

Para iniciar Adempiere se debe ubicar en el directorio:

cd /opt/Apps/Adempiere/utils

Ejecute el archivo RUN_Adempiere.sh

sh RUN_Adempiere.sh

![image alt text](img/image_2.png)

Inicialmente carga la ventana de login de adempiere.

Para configurar el servidor y la base de datos es necesario presionar el campo "Server".

![image alt text](img/image_3.png)

Donde mostrará una ventana con los campos requeridos para configuración:

![image alt text](img/image_4.png)

Para efecto de este curso se utilizará la siguiente configuración:

<table>
  <tr>
    <td> Campo</td>
    <td> Valor</td>
  </tr>
  <tr>
    <td> Database Type</td>
    <td> PostgreSQL</td>
  </tr>
  <tr>
    <td> Database Host</td>
    <td> localhost</td>
  </tr>
  <tr>
    <td> Database Port</td>
    <td> 5432</td>
  </tr>
  <tr>
    <td> Database Name</td>
    <td> adempiere</td>
  </tr>
  <tr>
    <td>  User / Password</td>
    <td>  adempiere</td>
  </tr>
</table>


Para verificar que la conexión ha sido exitosa se debe presionar el botón "Test Database". Al estar todo ok,debe guardar la configuración.

ADempiere maneja por defecto una serie de Usuarios:

<table>
  <tr>
    <td> Usuario</td>
    <td>Clave </td>
  </tr>
  <tr>
    <td>System</td>
    <td>System</td>
  </tr>
  <tr>
    <td> SuperUser </td>
    <td>System </td>
  </tr>
  <tr>
    <td> GardenAdmin    </td>
    <td>GardenAdmin   </td>
  </tr>
  <tr>
    <td> GardenUser</td>
    <td>GardenUser
</td>
  </tr>
</table>


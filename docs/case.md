**Caso Práctico de Estudio**

Para comenzar con ADempiere se utilizará el siguiente diagrama E-R como ejemplo, a fines didácticos  y uso durante todo el taller. 

**Reservación de Mesas:**

![image alt text](img/image_8.png)

En base al modelo se requiere:

1. Crear ventana de "Mesas" y "Asignación de Mesa".

2. Crear los clientes (Solo la pestaña principal de socio de negocio y cliente, se debe predeterminar el campo cliente en verdadero).

3. Modificar el campo "Cerrado" de la ventana  "Asignación de Mesa", el cual debe tener una lógica de solo lectura cuando esté en verdadero.

4. Crear Ventana para registrar mesonero (Pestaña principal socio del negocio, pestaña de empleado y pestaña de usuario).

5. Crear campo de "Asignación de Mesa" en "Orden de Venta" (Solo Mostrar las asignaciones de mesas abiertas que no estén asignadas a otras órdenes de venta completas o cerradas).

6. En la "Orden de Venta" solo debe mostrar el socio de negocio (Cliente) de la asignación de mesa y el mesonero de la asignación de mesa.

7. Agregar al Formato de impresión de la orden de venta la mesa donde se emitió la orden.

8. Crear reporte de pedidos por Mesa.

9. Crear reporte de Pedidos por Mesonero, indicando en qué mesa se atendió. 

